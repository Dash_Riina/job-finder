function makeRequest(func) {

    var request = new window.XMLHttpRequest();
    request.open('GET', 'OP.json', true);
    request.onload = function() {
        func(JSON.parse(request.responseText));
    }

    request.onerror= function(data) {
        console.log(data);
    }
    request.send();

}

function putMessage(vacancy) {

    var container = document.getElementById('container');
    container.innerHTML = "";
    for (var key in vacancy) {
        var div = document.createElement('div');
        div.innerHTML = '<a href="vacancy.html?id='+key+'">'+vacancy[key].VacancyName+'</a>'+'<h3>'+vacancy[key].VacancyCompany+'</h3>'+'<p>'+vacancy[key].VacancyDescription+'</p>';
        div.className = 'vacancy';
        container.appendChild(div);
    }
}

function putFullMessage(vacancy) {
    var container = document.getElementById('container');
    container.innerHTML = "";
    for (var key in vacancy) {
        var div = document.createElement('div');
        div.innerHTML = '<a href="vacancy.html?id='+key+'">'+vacancy[key].VacancyName+'</a>'+'<h3>'+vacancy[key].VacancyCompany+'</h3>'+'<p>'+vacancy[key].VacancyDescription+'</p>';// + full description
        div.className = 'vacancy';
        container.appendChild(div);
    }
}

function vacancySearch(vacancy){
    var input = document.getElementById('vacancy').value;
    var search_string = new RegExp(input, "i");
    for (var key in vacancy) {
        if(vacancy[key].VacancyName.search(search_string) == -1) {
            delete vacancy[key];
        }
    }
    putMessage(vacancy);
}

function parseQueryString() {
    var str = window.location.search;
    var objURL = {};

    str.replace(
        new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
        function( $0, $1, $2, $3 ){
            objURL[ $1 ] = $3;
        }
    );
    return objURL;
}


function fullDescription(vacancy) {
    var params = parseQueryString();
    for (var key in vacancy) {
        if(key != params["id"]) {
            delete vacancy[key];
        }
    }
    putFullMessage(vacancy);
}





